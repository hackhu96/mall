package com.hackhu.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.hackhu.common.Const;
import com.hackhu.common.ServerResponse;
import com.hackhu.dao.CategoryMapper;
import com.hackhu.dao.ProductMapper;
import com.hackhu.enums.ProductStatusEnum;
import com.hackhu.enums.ResponseCode;
import com.hackhu.pojo.Category;
import com.hackhu.pojo.Product;
import com.hackhu.service.ICategoryService;
import com.hackhu.service.IProductService;
import com.hackhu.utils.DateTimeUtil;
import com.hackhu.utils.PropertiesUtil;
import com.hackhu.vo.ProductDetailVO;
import com.hackhu.vo.ProductListVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("iProductService")
public class ProductServiceImpl implements IProductService {
    @Autowired
    private ProductMapper mapper;
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private ICategoryService iCategoryService;
    @Override
    public ServerResponse saveOrUpdateProduct(Product product) {
        if (product != null) {
            if (StringUtils.isBlank(product.getSubImage())) {
                String[] images = product.getSubImage().split(",");
                // 主图为辅图的第一张
                if (images.length > 0) {
                    product.setMainImage(images[0]);
                }
                if (product.getId() != null) {
                    int rowCount = mapper.updateByPrimaryKeySelective(product);
                    return rowCount == 0 ? ServerResponse.createByError("产品更新失败") : ServerResponse.createBySuccess("产品更新成功");
                } else {
                    int rowCount = mapper.insertSelective(product);
                    return rowCount == 0 ? ServerResponse.createByError("产品新增失败") : ServerResponse.createBySuccess("产品新增成功");
                }
            }
        }
        return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getMessage());
    }

    @Override
    public ServerResponse<String> updateProductStatus(Integer productId, Integer status) {
        if (productId == null || status == null) {
            return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getMessage());
        }
        Product product = new Product();
        product.setId(productId);
        product.setStatus(status);
        int rowCount = mapper.updateByPrimaryKeySelective(product);
        return rowCount == 0 ? ServerResponse.createByError("产品状态更新失败") : ServerResponse.createBySuccess("产品状态更新成功");
    }

    @Override
    public ServerResponse<ProductDetailVO> manageProductDetail(Integer productId) {
        if (productId == null) {
            return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getMessage());
        }
        Product product = mapper.selectByPrimaryKey(productId);
        if (product == null) {
            return ServerResponse.createByError("产品已下架或不存在");
        }
        return ServerResponse.createBySuccess(assembleProductDetailVO(product));
    }

    @Override
    public ServerResponse<PageInfo> getProductList(int pageNum, int pageSize) {
        // 初始化 pageHelper
        PageHelper.startPage(pageNum, pageSize);
        List<Product> products = mapper.selectList();
        List<ProductListVO> productListVOS = assembleProductListVO(products);
        PageInfo pageInfo = new PageInfo(products);
        pageInfo.setList(productListVOS);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse<PageInfo> searchProduct(String productName, Integer productId, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        if (StringUtils.isNotBlank(productName)) {
            productName = "%" + productName + "%";
        }
        List<Product> products = mapper.selectListByNameAndProductId(productName, productId);
        List<ProductListVO> productListVOS = assembleProductListVO(products);
        PageInfo pageInfo = new PageInfo(products);
        pageInfo.setList(productListVOS);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse<ProductDetailVO> getProductDetail(Integer productId) {
        if (productId == null) {
            return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getMessage());
        }
        Product product = mapper.selectByPrimaryKey(productId);
        if (product == null) {
            return ServerResponse.createByError("产品已下架或不存在");
        }
        if (product.getStatus() != ProductStatusEnum.ON_SALE.getCode()) {
            return ServerResponse.createBySuccess(ProductStatusEnum.TAKE_DOWN.getMsg());
        }
        return ServerResponse.createBySuccess(assembleProductDetailVO(product));
    }

    @Override
    public ServerResponse<PageInfo> getProductListByKeyWordAndCategory(String keyword, Integer categoryId, int pageNum, int pageSize,String orderBy) {
        if (keyword == null && categoryId == null) {
            return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getCode(), ResponseCode.ILLEGAL_ARGUMENT.getMessage());
        }
        PageHelper.startPage(pageNum, pageSize);
        List<Integer> categoryIdList = new ArrayList<>();
        if (categoryId != null) {
            Category category = categoryMapper.selectByPrimaryKey(categoryId);
            if (category == null && StringUtils.isBlank(keyword)) {
                List<ProductDetailVO> productDetailVOS = Lists.newArrayList();
                PageInfo pageInfo = new PageInfo(productDetailVOS);
                return ServerResponse.createBySuccess(pageInfo);
            }
            categoryIdList = iCategoryService.getCategoryAndDeepChildrenParallelCategory(categoryId).getData();
        }
        if (StringUtils.isNotBlank(keyword)) {
            keyword = "%" + keyword + "%";
        }
        // 排序处理
        if (Const.ProductListOrderBy.PRICE_ASC_DESC.contains(orderBy)) {
            String[] orderByArray = orderBy.split("_");
            PageHelper.orderBy(orderByArray[0] + " " + orderByArray[1]);

        }
        List<Product> products = mapper.selectByNameAndCategoryIds(StringUtils.isBlank(keyword) ? null : keyword,
                categoryIdList.size() == 0 ? null : categoryIdList);

        List<ProductListVO> productListVOS = assembleProductListVO(products);
        PageInfo pageInfo = new PageInfo(products);
        pageInfo.setList(productListVOS);
        return ServerResponse.createBySuccess(pageInfo);
    }

    // 将 List<ProductDetailVO> 转换为 List<ProductListVO>
    private List<ProductListVO> assembleProductListVO(List<Product> products){
        List<ProductListVO> productListVOS = new ArrayList<>();
        for (Product product : products) {
            ProductListVO productListVO = new ProductListVO();
            productListVO.setId(product.getId());
            productListVO.setCategoryId(product.getCategoryId());
            productListVO.setMainImage(product.getMainImage());
            productListVO.setMainImage(product.getMainImage());
            productListVO.setSubImages(product.getSubImage());
            productListVO.setName(product.getName());
            productListVO.setPrice(product.getPrice());
            productListVO.setStatus(product.getStatus());
            productListVO.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix", "http://img.hackhu.com/"));
        }
        return productListVOS;
    }
    // 将 Product 转换为 ProductDetailVO
    private ProductDetailVO assembleProductDetailVO(Product product){
        ProductDetailVO productDetailVO = new ProductDetailVO();
        productDetailVO.setId(product.getId());
        productDetailVO.setSubTitle(product.getSubtitle());
        productDetailVO.setPrice(product.getPrice());
        productDetailVO.setMainImage(product.getMainImage());
        productDetailVO.setSubImages(product.getSubImage());
        productDetailVO.setCategoryId(product.getCategoryId());
        productDetailVO.setDetail(product.getDetail());
        productDetailVO.setName(product.getName());
        productDetailVO.setStatus(product.getStatus());
        productDetailVO.setStock(product.getStock());

        productDetailVO.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix", "http://img.hackhu.com/"));

        Category category = categoryMapper.selectByPrimaryKey(product.getCategoryId());
        if (category == null) {
            // 默认为一级分类
            productDetailVO.setPatentCategory(0);
        } else {
            productDetailVO.setPatentCategory(category.getId());
        }
        productDetailVO.setCreateTime(DateTimeUtil.dateToStr(product.getCreateTime()));
        productDetailVO.setUpdateTime(DateTimeUtil.dateToStr(product.getUpdateTime()));

        return productDetailVO;
    }
}

package com.hackhu.service.impl;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.hackhu.common.Const;
import com.hackhu.common.ServerResponse;
import com.hackhu.dao.CartMapper;
import com.hackhu.dao.ProductMapper;
import com.hackhu.enums.ResponseCode;
import com.hackhu.pojo.Cart;
import com.hackhu.pojo.Product;
import com.hackhu.service.ICartService;
import com.hackhu.utils.BigDecimalUtil;
import com.hackhu.utils.PropertiesUtil;
import com.hackhu.vo.CartProductVO;
import com.hackhu.vo.CartVO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service("iCartService")
public class CartServiceImpl implements ICartService {
    @Autowired
    private CartMapper cartMapper;
    @Autowired
    private ProductMapper productMapper;

    @Override
    public ServerResponse<CartVO> add(Integer userId, Integer productId, Integer count) {
        if (productId == null || count == null) {
            return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getMessage());
        }
        Cart cart = cartMapper.selectByUserIdAndProductId(userId, productId);
        if (cart == null) { // 不在购物车中，新增一个产品的记录
            Cart cartItem = new Cart();
            cartItem.setQuantity(count);
            cartItem.setProductId(productId);
            cartItem.setChecked(Const.CartStatus.CHECKED);
            cartItem.setUserId(userId);
        } else {
            // 购物车中已存在，增加选中数量,更新数据
            count += cart.getQuantity();
            cart.setQuantity(count);
            cartMapper.updateByPrimaryKeySelective(cart);
        }
        CartVO cartVO = getCartVOLimit(userId);
        return ServerResponse.createBySuccess(cartVO);
    }

    @Override
    public ServerResponse<CartVO> update(Integer userId, Integer productId, Integer count) {
        if (productId == null || count == null) {
            return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getMessage());
        }
        Cart cart = cartMapper.selectByUserIdAndProductId(userId, productId);
        if (cart != null) {
            cart.setQuantity(count);
        }
        cartMapper.updateByPrimaryKeySelective(cart);
        return list(userId);
    }

    @Override
    public ServerResponse<CartVO> deleteProduct(Integer userId, String productIds) {
        // 切分字符串
        List<String> strings = Splitter.on(",").splitToList(productIds);
        if (CollectionUtils.isEmpty(strings)) {
            return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getCode(), ResponseCode.ILLEGAL_ARGUMENT.getMessage());
        }
        cartMapper.deleteByUserIdAndProductId(userId, strings);
        return list(userId);
    }

    @Override
    public ServerResponse<CartVO> list(Integer userId) {
        CartVO cartVO = getCartVOLimit(userId);
        return ServerResponse.createBySuccess(cartVO);
    }

    @Override
    public ServerResponse<CartVO> selectOrUnSelect(Integer userId,Integer productId, Integer checked) {
        cartMapper.checkOrUncheckedProduct(userId, productId, checked);
        return list(userId);
    }

    @Override
    public ServerResponse<CartVO> selectOrUnSelectAll(Integer userId, Integer checked) {
        cartMapper.checkOrUncheckedAllProduct(userId, checked);
        return list(userId);
    }

    @Override
    public ServerResponse<Integer> getCartProductCount(Integer userId) {
        if (userId == null) {
            return ServerResponse.createByError(0);
        }
        return ServerResponse.createBySuccess(cartMapper.selectCartProductCount(userId));
    }

    private CartVO getCartVOLimit(Integer userId) {
        CartVO cartVO = new CartVO();
        List<Cart> cartList = cartMapper.selectByUserId(userId);
        List<CartProductVO> cartProductVOList = Lists.newArrayList();
        BigDecimal cartTotalPrice = new BigDecimal("0");
        if (CollectionUtils.isNotEmpty(cartList)) {
            for (Cart cart : cartList) {
                CartProductVO cartProductVO = new CartProductVO();
                cartProductVO.setId(cart.getId());
                cartProductVO.setUserId(cart.getUserId());
                Product product = productMapper.selectByPrimaryKey(cart.getProductId());
                if (product != null) { // 产品不为空填充产品信息
                    cartProductVO.setProductMainImage(product.getMainImage());
                    cartProductVO.setProductName(product.getName());
                    cartProductVO.setProductPrice(product.getPrice());
                    cartProductVO.setProductStatus(product.getStatus());
                    // 判断是否超出库存
                    if (product.getStock() >= cart.getQuantity()) {
                        cartProductVO.setLimitQuantity(Const.CartStatus.LIMIT_NUM_SUCCESS);
                    } else {
                        // 设置有效库存
                        Integer stock = product.getStock();
                        cartProductVO.setLimitQuantity(Const.CartStatus.LIMIT_NUM_FAIL);
                        cartProductVO.setQuantity(stock);
                        Cart cartTemp = new Cart();
                        cartTemp.setId(cart.getId());
                        cartTemp.setQuantity(stock);
                        cartMapper.updateByPrimaryKeySelective(cartTemp);
                        cartProductVO.setQuantity(stock);
                    }
                    // 计算商品总价
                    cartProductVO.setProductTotalPrice(BigDecimalUtil.mul(product.getPrice().doubleValue(), cartProductVO.getProductTotalPrice().doubleValue()));
                    cartProductVO.setProductChecked(cart.getChecked());
                }
                // 更新购物车商品总价
                if (cart.getChecked() == Const.CartStatus.CHECKED) {
                    cartTotalPrice = BigDecimalUtil.add(cartTotalPrice.doubleValue(), cartProductVO.getProductTotalPrice().doubleValue());
                }
                cartProductVOList.add(cartProductVO);
            }
        }
        cartVO.setCartTotalPrice(cartTotalPrice);
        cartVO.setCartProductVOList(cartProductVOList);
        cartVO.setAllChecked(getAllCheckedStatus(userId));
        cartVO.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix"));
        return cartVO;
    }

    private boolean getAllCheckedStatus(Integer userId) {
        if (userId == null) {
            return false;
        }
        return cartMapper.selectCartProductCheckedStatusByUserId(userId) == 0;
    }
}

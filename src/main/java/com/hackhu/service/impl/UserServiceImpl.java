package com.hackhu.service.impl;

import com.hackhu.common.Const;
import com.hackhu.common.ServerResponse;
import com.hackhu.common.TokenCache;
import com.hackhu.dao.UserMapper;
import com.hackhu.enums.ResponseCode;
import com.hackhu.pojo.User;
import com.hackhu.service.IUserService;
import com.hackhu.utils.MD5Util;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service("iUserService")
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public ServerResponse login(String username, String password) {
        int result = userMapper.checkUserName(username);
        if (result == 0) {
            return ServerResponse.createByError("用户名不存在");
        }

        // 密码登陆 MD5
        String md5Password = MD5Util.MD5EncodeUtf8(password);

        User user = userMapper.selectLogin(username, md5Password);
        if (user == null) {
            return ServerResponse.createByError("密码错误");
        }
        // 将密码置空
        user.setPassword(StringUtils.EMPTY);

        return ServerResponse.createBySuccess("登陆成功", user);
    }

    @Override
    public ServerResponse<String> register(User user) {
        // 校验该用户是否已存在
        if (!(checkValid(user.getUsername(),Const.USERNAME).isSuccess()||
                checkValid(user.getUsername(),Const.EMAIL).isSuccess())) {
            return ServerResponse.createByError("该用户已存在请重写创建");
        }
        user.setRole(Const.Role.ROLE_CUSTOMER);
        // 密码加密
        user.setPassword(MD5Util.MD5EncodeUtf8(user.getPassword()));
        int result = userMapper.insert(user); // 在数据库中创建 user

        return result == 0 ? ServerResponse.createByError("注册失败") :
                ServerResponse.createBySuccess("注册成功");
    }

    @Override
    public ServerResponse<String> checkValid(String str, String type) {
        if (StringUtils.isNotBlank(type)) {
            // 开始校验
            if ((Const.USERNAME.equals(type) && userMapper.checkUserName(str) > 0) ||
                    (Const.EMAIL.equals(type) && userMapper.checkEmail(str) > 0)) {
                return ServerResponse.createByError("用户已存在");
            }else {
                return ServerResponse.createBySuccess("校验成功");
            }
        }
        return ServerResponse.createByError("参数错误");
    }

    @Override
    public ServerResponse<String> selectQuestion(String username){
        if (!checkValid(username, Const.USERNAME).isSuccess()) {
            return ServerResponse.createByError(ResponseCode.NO_EXIST.getMessage());
        }
        String question = userMapper.selectQuestionByUsername(username);
        return question == null ? ServerResponse.createByError("密保问题不存在") :
                ServerResponse.createBySuccess(question);
    }

    @Override
    public ServerResponse<String> checkedAnswer(String username,String question,String answer) {
        if (userMapper.checkAnswer(username, question, answer) == 0) {
            return ServerResponse.createByError("找回密码失败");
        }
        // 生成一个标识
        String forgotToken = UUID.randomUUID().toString();
        // 缓存 token
        TokenCache.setKey(Const.TOKEN_PREFIX + username, forgotToken);
        return ServerResponse.createBySuccess(forgotToken);
    }

    @Override
    public ServerResponse<String> forgotResetPassword(String username,String newPassword,String token){
        if (StringUtils.isBlank(token)) {
            return ServerResponse.createByError("参数错误，请先传递 token");
        }
        // 验证该用户是否存在
        ServerResponse validResponse = this.checkValid(username, Const.USERNAME);
        if (!validResponse.isSuccess()) {
            return ServerResponse.createByError(ResponseCode.NO_EXIST.getMessage());
        }
        String forgotToken = TokenCache.getKey(Const.TOKEN_PREFIX + username);
        // 验证 token
        if (!StringUtils.equals(forgotToken, token)) {
            return ServerResponse.createByError("参数非法，请传入正确 token");
        }
        String password = MD5Util.MD5EncodeUtf8(newPassword);
        int rowCount = userMapper.updatePasswordByUserName(username, password);
        if (rowCount > 0) {
            return ServerResponse.createBySuccess("密码找回成功");
        }
        return ServerResponse.createByError("密码找回失败");
    }

    @Override
    public ServerResponse<String> resetPassword(User user, String password, String newPassword){
        // 防止横向越权，判断密码与用户 id 是否一致
        int count = userMapper.checkPassword(MD5Util.MD5EncodeUtf8(password), user.getId());
        if (count == 0) {
            return ServerResponse.createByError("旧密码错误");
        }

        // 更新密码
        user.setPassword(MD5Util.MD5EncodeUtf8(newPassword));
        int rowCount = userMapper.updateByPrimaryKeySelective(user);
        if (rowCount > 0) {
            return ServerResponse.createBySuccess("密码重置成功");
        }
        return ServerResponse.createByError("密码重置失败");
    }

    @Override
    public ServerResponse<User> updateInformation(User user) {
        int rowCount = userMapper.checkEmailByUserId(user.getId(), user.getEmail());
        if (rowCount > 0) {
            return ServerResponse.createByError("邮箱和之前绑定一致，请输入新的邮箱");
        }
        User updateUser = new User();
        updateUser.setId(user.getId());
        updateUser.setEmail(user.getEmail());
        updateUser.setPhone(user.getPhone());
        updateUser.setQuestion(user.getQuestion());
        updateUser.setAnswer(user.getAnswer());
        rowCount = userMapper.updateByPrimaryKeySelective(updateUser);
        if (rowCount > 0) {
            return ServerResponse.createBySuccess("用户信息更新成功",updateUser);
        }
        return ServerResponse.createByError("用户信息更新失败");
    }

    @Override
    public ServerResponse<User> getInformation(Integer userId) {
        User user = userMapper.selectByPrimaryKey(userId);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NO_EXIST.getMessage());
        }
        return ServerResponse.createBySuccess(user);
    }

    @Override
    public ServerResponse<String> checkAdminRole(User user) {
        return user != null && user.getRole().intValue() == Const.Role.ROLE_ADMIN ?
                ServerResponse.createBySuccess() : ServerResponse.createByError();
    }

}

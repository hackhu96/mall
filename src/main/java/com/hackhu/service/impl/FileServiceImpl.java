package com.hackhu.service.impl;

import com.google.common.collect.Lists;
import com.hackhu.service.IFileService;
import com.hackhu.utils.FTPUtil;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import org.slf4j.Logger;

@Service("iFileService")
public class FileServiceImpl implements IFileService {

    private Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);

    public String upload(MultipartFile file, String path) {
        String fileName = file.getOriginalFilename();

        // 获取扩展名
        String fileExtensionName = fileName.substring(fileName.lastIndexOf(".") + 1);
        String uploadFileName = UUID.randomUUID().toString() + "." + fileExtensionName;
        logger.info("开始上传文件，上传文件名：{}，上传路径：{}，新文件名：{}", fileName, path, uploadFileName);
        File fileDir = new File(path);
        if (!fileDir.exists()) { // 判断文件夹是否存储
            fileDir.setWritable(true); // 设为可写
            fileDir.mkdirs();
        }
        File targetFile = new File(path, uploadFileName);
        try {
            file.transferTo(targetFile);
            // 文件上传成功
            // 将 targetFile 存入 ftp 服务器
            FTPUtil.uploadFile(Lists.newArrayList(targetFile));

            // 将 upload 目录下文件删除
            targetFile.delete();
        } catch (IOException e) {
            logger.error("文件上传出错 ", e);
            return null;
        }
        return targetFile.getName();
    }
}

package com.hackhu.service.impl;

import com.alipay.api.AlipayResponse;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.alipay.demo.trade.Main;
import com.alipay.demo.trade.config.Configs;
import com.alipay.demo.trade.model.ExtendParams;
import com.alipay.demo.trade.model.GoodsDetail;
import com.alipay.demo.trade.model.builder.AlipayTradePrecreateRequestBuilder;
import com.alipay.demo.trade.model.result.AlipayF2FPrecreateResult;
import com.alipay.demo.trade.service.AlipayMonitorService;
import com.alipay.demo.trade.service.AlipayTradeService;
import com.alipay.demo.trade.service.impl.AlipayMonitorServiceImpl;
import com.alipay.demo.trade.service.impl.AlipayTradeServiceImpl;
import com.alipay.demo.trade.service.impl.AlipayTradeWithHBServiceImpl;
import com.alipay.demo.trade.utils.ZxingUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hackhu.common.ServerResponse;
import com.hackhu.dao.*;
import com.hackhu.enums.*;
import com.hackhu.pojo.*;
import com.hackhu.service.IOrderService;
import com.hackhu.utils.BigDecimalUtil;
import com.hackhu.utils.DateTimeUtil;
import com.hackhu.utils.FTPUtil;
import com.hackhu.utils.PropertiesUtil;
import com.hackhu.vo.OrderDetailVO;
import com.hackhu.vo.OrderProductVO;
import com.hackhu.vo.OrderVO;
import com.hackhu.vo.ShippingVO;
import com.mysql.fabric.Server;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.math.BigDecimal;
import java.util.*;

@Service("iOrderService")
public class OrderServiceImpl implements IOrderService {

    @Autowired
    private OrderDetailMapper detailMapper;
    @Autowired
    private CartMapper cartMapper;
    @Autowired
    private ShippingMapper shippingMapper;

    private static Log log = LogFactory.getLog(Main.class);

    // 支付宝当面付2.0服务
    private static AlipayTradeService tradeService;

    // 支付宝当面付2.0服务（集成了交易保障接口逻辑）
    private static AlipayTradeService   tradeWithHBService;

    // 支付宝交易保障接口服务，供测试接口api使用，请先阅读readme.txt
    private static AlipayMonitorService monitorService;

    static {
        /** 一定要在创建AlipayTradeService之前调用Configs.init()设置默认参数
         *  Configs会读取classpath下的zfbinfo.properties文件配置信息，如果找不到该文件则确认该文件是否在classpath目录
         */
        Configs.init("zfbinfo.properties");

        /** 使用Configs提供的默认参数
         *  AlipayTradeService可以使用单例或者为静态成员对象，不需要反复new
         */
        tradeService = new AlipayTradeServiceImpl.ClientBuilder().build();

        // 支付宝当面付2.0服务（集成了交易保障接口逻辑）
        tradeWithHBService = new AlipayTradeWithHBServiceImpl.ClientBuilder().build();

        /** 如果需要在程序中覆盖Configs提供的默认参数, 可以使用ClientBuilder类的setXXX方法修改默认参数 否则使用代码中的默认设置 */
        monitorService = new AlipayMonitorServiceImpl.ClientBuilder()
                .setGatewayUrl("http://mcloudmonitor.com/gateway.do").setCharset("GBK")
                .setFormat("json").build();
    }

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderDetailMapper orderDetailMapper;
    @Autowired
    private PayInfoMapper payInfoMapper;
    @Autowired
    private ProductMapper productMapper;

    @Override
    public ServerResponse pay(Long orderNo, Integer userId, String path) {
        Map<String, String> resultMap = Maps.newHashMap();
        Order order = orderMapper.selectByUserIdAndOrderNo(userId, orderNo);
        if (order == null) {
            return ServerResponse.createByError("用户并没有该订单");
        }
        resultMap.put("orderNo", String.valueOf(order.getOrderNo()));

        // (必填) 商户网站订单系统中唯一订单号，64个字符以内，只能包含字母、数字、下划线，
        // 需保证商户系统端不能重复，建议通过数据库sequence生成，
        String outTradeNo = "tradeprecreate" + System.currentTimeMillis()
                + (long) (Math.random() * 10000000L);

        // (必填) 订单标题，粗略描述用户的支付目的。如“xxx品牌xxx门店当面付扫码消费”
        String subject = new StringBuilder().append("mall 扫码支付，订单号: ").append(outTradeNo).toString();

        // (必填) 订单总金额，单位为元，不能超过1亿元
        // 如果同时传入了【打折金额】,【不可打折金额】,【订单总金额】三者,则必须满足如下条件:【订单总金额】=【打折金额】+【不可打折金额】
        String totalAmount = order.getPayment().toString();

        // (可选) 订单不可打折金额，可以配合商家平台配置折扣活动，如果酒水不参与打折，则将对应金额填写至此字段
        // 如果该值未传入,但传入了【订单总金额】,【打折金额】,则该值默认为【订单总金额】-【打折金额】
        String undiscountableAmount = "0";

        // 卖家支付宝账号ID，用于支持一个签约账号下支持打款到不同的收款账号，(打款到sellerId对应的支付宝账号)
        // 如果该字段为空，则默认为与支付宝签约的商户的PID，也就是appid对应的PID
        String sellerId = "";

        // 订单描述，可以对交易或商品进行一个详细地描述，比如填写"购买商品2件共15.00元"
        String body = new StringBuilder().append("订单").append(orderNo).append("购买商品共: ").append(totalAmount).append(" 元").toString();

        // 商户操作员编号，添加此参数可以为商户操作员做销售统计
        String operatorId = "test_operator_id";

        // (必填) 商户门店编号，通过门店号和商家后台可以配置精准到门店的折扣信息，详询支付宝技术支持
        String storeId = "test_store_id";

        // 业务扩展参数，目前可添加由支付宝分配的系统商编号(通过setSysServiceProviderId方法)，详情请咨询支付宝技术支持
        ExtendParams extendParams = new ExtendParams();
        extendParams.setSysServiceProviderId("2088100200300400500");

        // 支付超时，定义为120分钟
        String timeoutExpress = "120m";

        // 商品明细列表，需填写购买商品详细信息，
        List<GoodsDetail> goodsDetailList = new ArrayList<GoodsDetail>();

        List<OrderDetail> orderDetailList = detailMapper.getByOrderNoAndUserId(orderNo, userId);
        for (OrderDetail orderDetail : orderDetailList) {
            // 创建一个商品信息，参数含义分别为商品id（使用国标）、名称、单价（单位为分）、数量，如果需要添加商品类别，详见GoodsDetail
            GoodsDetail goodsDetail = GoodsDetail.newInstance(orderDetail.getProductId().toString(), orderDetail.getProductName(),
                    BigDecimalUtil.mul(orderDetail.getCurrentInitPrice().doubleValue(), new Double(100).doubleValue()).longValue(),
                    orderDetail.getQuantity());
            // 创建好一个商品后添加至商品明细列表
            goodsDetailList.add(goodsDetail);
        }


        // 创建扫码支付请求builder，设置请求参数
        AlipayTradePrecreateRequestBuilder builder = new AlipayTradePrecreateRequestBuilder()
                .setSubject(subject).setTotalAmount(totalAmount).setOutTradeNo(outTradeNo)
                .setUndiscountableAmount(undiscountableAmount).setSellerId(sellerId).setBody(body)
                .setOperatorId(operatorId).setStoreId(storeId).setExtendParams(extendParams)
                .setTimeoutExpress(timeoutExpress)
                .setNotifyUrl(PropertiesUtil.getProperty("alipay.callback.url"))//支付宝服务器主动通知商户服务器里指定的页面http路径,根据需要设置
                .setGoodsDetailList(goodsDetailList);

        AlipayF2FPrecreateResult result = tradeService.tradePrecreate(builder);
        switch (result.getTradeStatus()) {
            case SUCCESS:
                log.info("支付宝预下单成功: )");

                AlipayTradePrecreateResponse response = result.getResponse();
                dumpResponse(response);

                File folder = new File(path);
                if (folder.exists()) {
                    folder.setWritable(true);
                    folder.mkdir();
                }
                // 需要修改为运行机器上的路径
                String qrParh = String.format(path+"/qr-%s.png",
                        response.getOutTradeNo());
                String qrFileName = String.format("qr-%s.png", response.getOutTradeNo());

                ZxingUtils.getQRCodeImge(response.getQrCode(), 256, qrParh);
                File targetFile = new File(path, qrFileName);
                FTPUtil.uploadFile(Lists.newArrayList(targetFile));
                log.info("filePath:" + qrParh);
                String qrUrl = PropertiesUtil.getProperty("ftp.server.http.prefix") + targetFile.getName();
                log.info("qrUrl:" + qrUrl);
                //                ZxingUtils.getQRCodeImge(response.getQrCode(), 256, filePath);
                return ServerResponse.createBySuccess(resultMap);

            case FAILED:
                log.error("支付宝预下单失败!!!");
                return ServerResponse.createByError("支付宝预下单失败!!!");
            case UNKNOWN:
                log.error("系统异常，预下单状态未知!!!");
                return ServerResponse.createByError("系统异常，预下单状态未知!!!");

            default:
                log.error("不支持的交易状态，交易返回异常!!!");
                return ServerResponse.createByError("不支持的交易状态，交易返回异常!!!");
        }

    }

    @Override
    public ServerResponse aliCallback(Map<String, String> params) {
        Long orderNo = Long.parseLong(params.get("out_trade_no"));
        String tradeNo = params.get("trade_no");
        int tradeStatus = Integer.parseInt(params.get("trade_status")) ;
        Order order = orderMapper.selectByOrderNo(orderNo);
        if (order == null) {
            return ServerResponse.createByError("非正确订单，回调忽略");
        }
        if (order.getStatus() >= OrderStatusEnum.PAID.getCode()) {
            return ServerResponse.createBySuccess("支付宝重复调用");
        }
        if (TradeStatusEnum.TRADE_SUCCESS.getCode() == tradeStatus) {
            order.setPaymentTime(DateTimeUtil.strToDate(params.get("gmt_payment")));
            order.setStatus(OrderStatusEnum.PAID.getCode());
            orderMapper.updateByPrimaryKeySelective(order);
        }
        PayInfo payInfo = new PayInfo();
        payInfo.setUserId(order.getUserId());
        payInfo.setOrderNo(order.getOrderNo());
        payInfo.setPayPlatform(PayPlatformEnum.ALIPAY.getCode());
        payInfo.setPayStatus(order.getStatus().toString());
        payInfoMapper.insert(payInfo);
        return ServerResponse.createBySuccess();
    }

    @Override
    public ServerResponse<Boolean> queryOrderPayStatus(Integer userId, Long orderNo) {
        Order order = orderMapper.selectByUserIdAndOrderNo(userId, orderNo);
        if (order == null) {
            return ServerResponse.createByError("用户并没有该订单");
        }
        return order.getStatus() >= OrderStatusEnum.PAID.getCode() ? ServerResponse.createBySuccess(true) : ServerResponse.createByError(false);
    }

    @Override
    public ServerResponse createOrder(Integer userId, Integer shippingId) {
        // 获取购物车数据
        List<Cart> carts = cartMapper.selectCheckedCartByUserId(userId);
        // 计算订单总价
        ServerResponse<List<OrderDetail>> serverResponse = getCartOrderDetail(userId, carts);
        if (!serverResponse.isSuccess()) {
            return serverResponse;
        }
        List<OrderDetail> orderDetails = serverResponse.getData();
        BigDecimal payment = getOrderTotalPrice(orderDetails);
        // 生成订单
        Order order = assembleOrder(userId, shippingId, payment);
        if (order == null) {
            return ServerResponse.createByError("订单生产失败");
        }
        if (CollectionUtils.isEmpty(orderDetails)) {
            return ServerResponse.createByError("购物车为空");
        }
        for (OrderDetail orderDetail : orderDetails) {
            orderDetail.setOrderNo(order.getOrderNo());
        }
        orderDetailMapper.batchInsert(orderDetails);
        // 减少库存、清空购物车
        reduceProduceStock(orderDetails);
        cleanCart(carts);
        OrderVO orderVO = assembleOrderVO(order, orderDetails);
        return ServerResponse.createBySuccess(orderVO);
    }

    @Override
    public ServerResponse<String> cancel(Integer userId, Long orderNo) {
        Order order = orderMapper.selectByOrderNo(orderNo);
        if (order == null) {
            return ServerResponse.createByError("订单不存在");
        }
        if (order.getStatus() != OrderStatusEnum.NO_PAY.getCode()) {
            return ServerResponse.createByError("订单已付款无法取消");
        }
        Order updateOrder = new Order();
        updateOrder.setId(order.getId());
        updateOrder.setStatus(OrderStatusEnum.CANCELED.getCode());
        int rowCount = orderMapper.updateByPrimaryKeySelective(updateOrder);
        return rowCount>0?ServerResponse.createBySuccess("订单取消成功"):ServerResponse.createByError("订单取消失败");
    }

    @Override
    public ServerResponse getOrderCartProduct(Integer userId) {
        OrderProductVO orderProductVO = new OrderProductVO();
        List<Cart> carts = cartMapper.selectCheckedCartByUserId(userId);
        ServerResponse<List<OrderDetail>> serverResponse = getCartOrderDetail(userId,carts);
        if (!serverResponse.isSuccess()) {
            return serverResponse;
        }
        List<OrderDetail> orderDetails = (List<OrderDetail>) serverResponse.getData();
        List<OrderDetailVO> orderDetailVOS = Lists.newArrayList();
        BigDecimal payment = new BigDecimal("0");
        for (OrderDetail orderDetail : orderDetails) {
            orderDetailVOS.add(assembleOrderDetail(orderDetail));
            payment = BigDecimalUtil.add(payment.doubleValue(), orderDetail.getTotalPrice().doubleValue());
        }
        orderProductVO.setOrderDetailVOS(orderDetailVOS);
        orderProductVO.setProductTotalPrice(payment);
        orderProductVO.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix"));
        return ServerResponse.createBySuccess(orderProductVO);
    }

    @Override
    public ServerResponse<OrderVO> getOrderDetail(Integer userId, Long orderNo) {
        Order order = orderMapper.selectByOrderNo(orderNo);
        if (order == null) {
            return ServerResponse.createByError("订单不存在");
        }
        List<OrderDetail> orderDetails = orderDetailMapper.getByOrderNoAndUserId(orderNo, userId);
        OrderVO orderVO = assembleOrderVO(order, orderDetails);
        return ServerResponse.createBySuccess(orderVO);
    }

    @Override
    public ServerResponse<PageInfo> getOrderList(Integer userId,int pageNum,int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Order> orders = orderMapper.selectByUserId(userId);
        PageInfo pageInfo = new PageInfo(orders);
        List<OrderVO> orderVOS = assembleOrderVOList(orders, userId);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse<PageInfo> manageList(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Order> orders = orderMapper.selectAll();
        List<OrderVO> orderVOS = assembleOrderVOList(orders, null);
        PageInfo pageInfo = new PageInfo(orders);
        pageInfo.setList(orderVOS);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse<OrderVO> orderDetail(Long orderNo) {
        Order order = orderMapper.selectByOrderNo(orderNo);
        if (order != null) {
            List<OrderDetail> orderDetails = orderDetailMapper.getByOrderNo(orderNo);
            OrderVO orderVO = assembleOrderVO(order, orderDetails);
            return ServerResponse.createBySuccess(orderVO);
        }
        return ServerResponse.createByError("订单不存在");
    }

    @Override
    public ServerResponse<PageInfo> manageSearch(Long orderNo, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Order order = orderMapper.selectByOrderNo(orderNo);
        if (order != null) {
            PageInfo pageInfo = new PageInfo(Lists.newArrayList(order));
            List<OrderDetail> orderDetails = orderDetailMapper.getByOrderNo(orderNo);
            pageInfo.setList(Lists.newArrayList(assembleOrderVO(order, orderDetails)));
            return ServerResponse.createBySuccess(pageInfo);
        }
        return ServerResponse.createByError("订单不存在");
    }

    @Override
    public ServerResponse<String> manageSendGoods(Long orderNo) {
        Order order = orderMapper.selectByOrderNo(orderNo);
        if (order != null){
            return ServerResponse.createByError("订单不存在");
        }
        if (order.getStatus() != OrderStatusEnum.PAID.getCode()) {
            return ServerResponse.createByError("订单状态错误");
        }
        Order updateOrder = new Order();
        updateOrder.setId(order.getId());
        updateOrder.setSendTime(new Date());
        updateOrder.setStatus(OrderStatusEnum.SHIPPING.getCode());
        orderMapper.updateByPrimaryKeySelective(order);
        return ServerResponse.createBySuccess("发货成功");
    }

    private List<OrderVO> assembleOrderVOList(List<Order> orders, Integer userId) {
        List<OrderVO> orderVOS = Lists.newArrayList();
        for (Order order : orders) {
            List<OrderDetail> orderDetails = Lists.newArrayList();
            if (userId == null) { // 管理员
                orderDetails = orderDetailMapper.getByOrderNo(order.getOrderNo());
            } else {
                orderDetails = orderDetailMapper.getByOrderNoAndUserId(order.getOrderNo(), userId);
            }
            OrderVO orderVO = assembleOrderVO(order, orderDetails);
            orderVOS.add(orderVO);
        }
        return orderVOS;
    }
    private OrderVO assembleOrderVO(Order order, List<OrderDetail> orderDetails) {
        OrderVO orderVO = new OrderVO();
        orderVO.setOrderNo(order.getOrderNo());
        orderVO.setPayment(order.getPayment());
        orderVO.setPaymentType(order.getPaymentType());
        orderVO.setPaymentTypeDesc(PaymentTypeEnum.codeOf(order.getPaymentType()).getMsg());
        orderVO.setPostage(order.getPostage());
        orderVO.setStatus(order.getStatus());
        orderVO.setStatusDesc(OrderStatusEnum.codeOf(order.getStatus()).getMsg());
        Shipping shipping = shippingMapper.selectByPrimaryKey(order.getShippingId());
        if (shipping != null) {
            orderVO.setShippingId(shipping.getId());
            orderVO.setReceiverName(shipping.getReceiverName());
            orderVO.setShippingVO(assembleShippingVO(shipping));
        }
        orderVO.setPaymentTime(DateTimeUtil.dateToStr(order.getPaymentTime()));
        orderVO.setSendTime(DateTimeUtil.dateToStr(order.getSendTime()));
        orderVO.setEndTime(DateTimeUtil.dateToStr(order.getEndTime()));
        orderVO.setCreateTime(DateTimeUtil.dateToStr(order.getCreateTime()));
        orderVO.setCloseTime(DateTimeUtil.dateToStr(order.getCloseTime()));
        List<OrderDetailVO> orderDetailVOS = Lists.newArrayList();
        for (OrderDetail orderDetail : orderDetails) {
            orderDetailVOS.add(assembleOrderDetail(orderDetail));
        }
        return orderVO;
    }

    private OrderDetailVO assembleOrderDetail(OrderDetail orderDetail) {
        OrderDetailVO orderDetailVO = new OrderDetailVO();
        orderDetailVO.setOrderNo(orderDetail.getOrderNo());
        orderDetailVO.setCreateTime(DateTimeUtil.dateToStr(orderDetail.getCreateTime()));
        orderDetailVO.setProductId(orderDetail.getProductId());
        orderDetailVO.setProductName(orderDetail.getProductName());
        orderDetailVO.setCurrentInitPrice(orderDetail.getCurrentInitPrice());
        orderDetailVO.setQuantity(orderDetail.getQuantity());
        orderDetailVO.setTotalPrice(orderDetail.getTotalPrice());
        orderDetailVO.setProductImage(orderDetail.getProductImage());
        return orderDetailVO;
    }

    private ShippingVO assembleShippingVO(Shipping shipping) {
        ShippingVO shippingVO = new ShippingVO();
        shippingVO.setReceiverName(shipping.getReceiverName());
        shippingVO.setReceiverAddress(shipping.getReceiverAddress());
        shippingVO.setReceiverProvince(shipping.getReceiverProvince());
        shippingVO.setReceiverCity(shipping.getReceiverCity());
        shippingVO.setReceiverDistrict(shipping.getReceiverDistrict());
        shippingVO.setReceiverMobile(shipping.getReceiverMobie());
        shippingVO.setReceiverTelephone(shipping.getReceiverTelephone());
        shippingVO.setReceiverZip(shipping.getReceiverZip());
        return shippingVO;
    }

    private void cleanCart(List<Cart> carts){
        for (Cart cart : carts) {
            cartMapper.deleteByPrimaryKey(cart.getId());
        }
    }

    private void reduceProduceStock(List<OrderDetail> orderDetails) {
        for (OrderDetail orderDetail : orderDetails) {
            Product product = productMapper.selectByPrimaryKey(orderDetail.getProductId());
            product.setStock(product.getStock() - orderDetail.getQuantity());
            productMapper.updateByPrimaryKeySelective(product);
        }
    }
    // 装配订单
    private Order assembleOrder(Integer userId, Integer shippingId, BigDecimal payment) {
        Order order = new Order();
        long orderNo = generateOrderNo();
        order.setOrderNo(orderNo);
        order.setStatus(OrderStatusEnum.NO_PAY.getCode());
        order.setPostage(0);
        order.setPaymentType(PaymentTypeEnum.ONLINE_PAY.getCode());
        order.setUserId(userId);
        order.setShippingId(shippingId);
        int rowCount = orderMapper.insert(order);
        return rowCount > 0 ? order : null;
    }
    // 计算订单总价
    private BigDecimal getOrderTotalPrice(List<OrderDetail> orderDetails) {
        BigDecimal payment = new BigDecimal("0");
        for (OrderDetail orderDetail : orderDetails) {
            payment = BigDecimalUtil.add(payment.doubleValue(), orderDetail.getTotalPrice().doubleValue());
        }
        return payment;
    }

    // 生成订单号
    private long generateOrderNo(){
        long currentTime = System.currentTimeMillis();
        return currentTime + new Random().nextInt(100); // 使用 Random，防止并发时生成一致订单
    }
    private ServerResponse<List<OrderDetail>> getCartOrderDetail(Integer userId, List<Cart> carts) {
        ArrayList<OrderDetail> orderDetails = Lists.newArrayList();
        if (CollectionUtils.isEmpty(carts)) {
            return ServerResponse.createByError("购物车为空");
        }
        // 校验购物车数据：产品状态、数量
        for (Cart cart : carts) {
            Product product = productMapper.selectByPrimaryKey(cart.getProductId());
            if (product.getStatus() != ProductStatusEnum.ON_SALE.getCode()) {
                return ServerResponse.createByError("产品：" + product.getName() + "为未上架状态");
            }
            if (product.getStock() < cart.getQuantity()) {
                return ServerResponse.createByError("产品：" + product.getName() + "库存不足");
            }
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setUserId(cart.getUserId());
            orderDetail.setProductId(product.getId());
            orderDetail.setProductImage(product.getMainImage());
            orderDetail.setCurrentInitPrice(product.getPrice());
            orderDetail.setTotalPrice(BigDecimalUtil.mul(cart.getQuantity(), product.getPrice().doubleValue()));
            orderDetails.add(orderDetail);
        }
        return ServerResponse.createBySuccess(orderDetails);
    }

    // 简单打印应答
    private void dumpResponse(AlipayResponse response) {
        if (response != null) {
            log.info(String.format("code:%s, msg:%s", response.getCode(), response.getMsg()));
            if (StringUtils.isNotEmpty(response.getSubCode())) {
                log.info(String.format("subCode:%s, subMsg:%s", response.getSubCode(),
                        response.getSubMsg()));
            }
            log.info("body:" + response.getBody());
        }
    }
}

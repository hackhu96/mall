package com.hackhu.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.hackhu.common.ServerResponse;
import com.hackhu.dao.CategoryMapper;
import com.hackhu.enums.ResponseCode;
import com.hackhu.pojo.Category;
import com.hackhu.service.ICategoryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Set;

@Service("iCategoryService")
public class CategoryServiceImpl implements ICategoryService {
    private Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);
    @Autowired
    private CategoryMapper mapper;
    @Override
    public ServerResponse<String> addCategory(Integer parentId, String categoryName) {
        if (parentId == null || StringUtils.isBlank(categoryName)) {
            return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getMessage());
        }
        Category category = new Category();
        category.setName(categoryName);
        category.setParentId(parentId);
        category.setStatus(true);
        int rowCount = mapper.insertSelective(category);
        return rowCount == 0 ? ServerResponse.createByError("商品类别添加失败") : ServerResponse.createBySuccess("商品类别添加成功");
    }

    @Override
    public ServerResponse<String> updateCategoryName(Integer categoryId, String categoryName) {
        if (categoryId == null || StringUtils.isBlank(categoryName) || mapper.selectByPrimaryKey(categoryId) == null) {
            return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getMessage());
        }
        Category category = new Category();
        category.setName(categoryName);
        category.setId(categoryId);
        int rowCount = mapper.updateByPrimaryKeySelective(category);
        return rowCount > 0 ? ServerResponse.createBySuccess("分类名更新成功") : ServerResponse.createByError("分类名更新失败");
    }

    @Override
    public ServerResponse<List<Category>> getCategoryChildByParentId(Integer parentId) {
        List<Category> categoryList = mapper.selectCategoryChildByParentId(parentId);
        if (CollectionUtils.isEmpty(categoryList)) {
            logger.info("未找到子分类");
        }
        return ServerResponse.createBySuccess(categoryList);
    }

    @Override // 递归查询子分类
    public ServerResponse<List<Integer>> getCategoryAndDeepChildrenParallelCategory(Integer parentId) {
        Set<Category> categorySet = Sets.newHashSet();
        Set<Category> childCategory = findChildCategory(categorySet, parentId);
        List<Integer> list = Lists.newArrayList();
        if (parentId != null) {
            for (Category category : childCategory) {
                list.add(category.getId());
            }
        }
        return ServerResponse.createBySuccess(list);
    }

    // 递归获取子分类
    private Set<Category> findChildCategory(Set<Category> categories, Integer categoryId) {
        Category category = mapper.selectByPrimaryKey(categoryId);
        if (category != null) {
            categories.add(category);
        }
        List<Category> categoryList = mapper.selectCategoryChildByParentId(categoryId);
        for (Category c : categoryList) {
            findChildCategory(categories, c.getId());
        }
        return categories;
    }
}

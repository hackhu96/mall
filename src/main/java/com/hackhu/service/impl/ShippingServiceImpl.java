package com.hackhu.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.hackhu.common.ServerResponse;
import com.hackhu.dao.ShippingMapper;
import com.hackhu.pojo.Shipping;
import com.hackhu.service.IShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@Service("iShippingService")
public class ShippingServiceImpl implements IShippingService {

    @Autowired
    private ShippingMapper shippingMapper;

    @Override
    public  ServerResponse  add(Integer userId, Shipping shipping) {
        shipping.setUserId(userId);
        int rowCount = shippingMapper.insertSelective(shipping);
        if (rowCount > 0) {
            Map result = Maps.newHashMap();
            result.put("shipping", shipping.getId());
            return ServerResponse.createBySuccess("新建地址成功", result);
        }
        return ServerResponse.createByError("新建地址失败");
    }

    @Override
    public ServerResponse<String> delete(Integer userId, Integer shippingId) {
        int rowCount = shippingMapper.deleteByUserIdAndShippingId(userId, shippingId);
        return rowCount == 0 ? ServerResponse.createByError("删除地址失败") :
                ServerResponse.createByError("删除地址成功");
    }

    @Override
    public ServerResponse<Shipping> select(Integer userId, Integer shippingId) {
        Shipping shipping = shippingMapper.selectByUserIdAndShippingId(userId, shippingId);
        return shipping == null ? ServerResponse.createByError("地址查询失败") : ServerResponse.createBySuccess("地址查询成功", shipping);
    }

    @Override
    public ServerResponse<PageInfo> list(Integer userId, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Shipping> shippings = shippingMapper.selectByUserId(userId);
        PageInfo pageInfo = new PageInfo(shippings);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse update(Integer userId,Shipping shipping) {
        shipping.setUserId(userId);
        if (shippingMapper.updateByUserIdAndShippingId(shipping) > 0) {
            Shipping shipping1 = shippingMapper.selectByPrimaryKey(shipping.getId());
            return ServerResponse.createBySuccess("地址更新成功", shipping1);
        }
        return ServerResponse.createByError("更新失败 ");
    }
}

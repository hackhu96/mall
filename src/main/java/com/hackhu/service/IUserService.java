package com.hackhu.service;

import com.hackhu.common.ServerResponse;
import com.hackhu.pojo.User;

public interface IUserService {
    ServerResponse login(String name, String password);

    ServerResponse<String> register(User user);

    ServerResponse<String> checkValid(String str, String type);

    ServerResponse<String> selectQuestion(String username);

    ServerResponse<String> checkedAnswer(String username, String question, String answer);

    ServerResponse<String> forgotResetPassword(String username, String newPassword, String token);

    ServerResponse<String> resetPassword(User user, String password, String newPassword);

    ServerResponse<User> updateInformation(User user);

    ServerResponse<User> getInformation(Integer userId);

    ServerResponse<String> checkAdminRole(User user);

}

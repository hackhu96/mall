package com.hackhu.service;

import com.hackhu.common.ServerResponse;
import com.hackhu.pojo.Category;

import java.util.List;

public interface ICategoryService {
    ServerResponse<String> addCategory(Integer parentId, String categoryName);

    ServerResponse<String> updateCategoryName(Integer categoryId, String categoryName);

    ServerResponse<List<Category>> getCategoryChildByParentId(Integer parentId);

    ServerResponse<List<Integer>> getCategoryAndDeepChildrenParallelCategory(Integer parentId);


}

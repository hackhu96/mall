package com.hackhu.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.hackhu.common.ServerResponse;
import com.hackhu.pojo.Product;
import com.hackhu.vo.ProductDetailVO;
import com.hackhu.vo.ProductListVO;

import java.util.List;

public interface IProductService {
    ServerResponse saveOrUpdateProduct(Product product);

    ServerResponse<String> updateProductStatus(Integer productId, Integer status);

    ServerResponse<ProductDetailVO> manageProductDetail(Integer productId);

    ServerResponse<PageInfo> getProductList(int pageNum, int pageSize);

    ServerResponse<PageInfo> searchProduct(String productName, Integer productId, int pageNum, int pageSize);

    ServerResponse<ProductDetailVO> getProductDetail(Integer productId);

    ServerResponse<PageInfo> getProductListByKeyWordAndCategory(String keyword,Integer categoryId,int pageNum, int pageSize,String orderBy);
}

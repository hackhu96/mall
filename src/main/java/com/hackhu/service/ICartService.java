package com.hackhu.service;

import com.hackhu.common.ServerResponse;
import com.hackhu.vo.CartVO;

public interface ICartService {
    public ServerResponse<CartVO> add(Integer userId, Integer productId, Integer count);

    public ServerResponse<CartVO> update(Integer userId, Integer productId, Integer count);

    public ServerResponse<CartVO> deleteProduct(Integer userId, String productIds);

    public ServerResponse<CartVO> list(Integer userId);

    public ServerResponse<CartVO> selectOrUnSelect(Integer userId,Integer productId, Integer checked);

    public ServerResponse<CartVO> selectOrUnSelectAll(Integer userId, Integer checked);

    public ServerResponse<Integer> getCartProductCount(Integer userId);


}

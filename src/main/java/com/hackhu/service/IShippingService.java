package com.hackhu.service;

import com.github.pagehelper.PageInfo;
import com.hackhu.common.ServerResponse;
import com.hackhu.pojo.Shipping;

public interface IShippingService {
    ServerResponse add(Integer userId, Shipping shipping);

    ServerResponse<String> delete(Integer userId, Integer shippingId);

    ServerResponse<Shipping> select(Integer userId, Integer shippingId);

    ServerResponse<PageInfo> list(Integer userId,int pageNum,int pageSize);

    ServerResponse update(Integer userId,Shipping shipping);
}

package com.hackhu.dao;

import com.hackhu.pojo.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    int checkUserName(String username);

    int checkEmail(String email);

    User selectLogin(@Param("username") String username, @Param("password") String password);

    String selectQuestionByUsername(String username);

    int checkAnswer(@Param("username") String username,@Param("question") String question,@Param("answer") String answer);

    int checkPassword(@Param("password") String password,@Param("userId") Integer userId);

    int updatePasswordByUserName(@Param("username") String username, @Param("newPassword") String newPassword);

    int checkEmailByUserId(@Param("userId") Integer userId, @Param("email") String email);
}
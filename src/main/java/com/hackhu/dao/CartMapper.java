package com.hackhu.dao;

import com.google.common.collect.Lists;
import com.hackhu.pojo.Cart;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CartMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Cart record);

    int insertSelective(Cart record);

    Cart selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Cart record);

    int updateByPrimaryKey(Cart record);

    Cart selectByUserIdAndProductId(Integer userId, Integer productId);

    List<Cart> selectByUserId(Integer userId);

    int selectCartProductCheckedStatusByUserId(Integer userId);

    int deleteByUserIdAndProductId(@Param("userId") Integer userId, @Param("productList") List<String> productList);

    int checkOrUncheckedAllProduct(@Param("userId") Integer userId,Integer checked);

    int checkOrUncheckedProduct(@Param("userId") Integer userId,@Param("productId") Integer productId,Integer checked);

    int selectCartProductCount(@Param("userId") Integer userId);

    List<Cart> selectCheckedCartByUserId(Integer userId);
}
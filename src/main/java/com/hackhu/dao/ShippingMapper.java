package com.hackhu.dao;

import com.hackhu.pojo.Shipping;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShippingMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Shipping record);

    int insertSelective(Shipping record);

    Shipping selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Shipping record);

    int updateByPrimaryKey(Shipping record);

    int deleteByUserIdAndShippingId(@Param("userId") Integer userId, @Param("ShippingId") Integer shippingId);

    Shipping selectByUserIdAndShippingId(@Param("userId") Integer userId, @Param("ShippingId") Integer shippingId);

    int updateByUserIdAndShippingId(@Param("record") Shipping record);

    List<Shipping> selectByUserId(@Param("userId") Integer userId);

}
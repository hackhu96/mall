package com.hackhu.controller;

import com.hackhu.common.Const;
import com.hackhu.common.ServerResponse;
import com.hackhu.enums.ResponseCode;
import com.hackhu.pojo.User;
import com.hackhu.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/user/")
public class UserController {
    @Autowired
    private IUserService iUserService;

    // 用户登陆
    @RequestMapping(value = "login.do", method = RequestMethod.POST)
    public ServerResponse<User> login(String username, String password, HttpSession session){
        ServerResponse<User> response = iUserService.login(username, password);
        if (response != null) { // 登录成功，设置 session
            session.setAttribute(Const.CURRENT_USER, response.getData());
        }
        return response;
    }

    // 用户退出登录
    @RequestMapping(value = "logout.do", method = RequestMethod.GET)
    public ServerResponse<String> logout(HttpSession session) {
        session.removeAttribute(Const.CURRENT_USER);
        return ServerResponse.createBySuccess();
    }

    // 用户注册
    @RequestMapping(value = "register.do",method = RequestMethod.POST)
    public ServerResponse<String> register(User user){
        return iUserService.register(user);
    }

    // 用户校验
    @RequestMapping(value = "checkValid.do",method = RequestMethod.POST)
    public ServerResponse<String> checkValid(String str,String type){
        return iUserService.checkValid(str, type);
    }

    // 获取用户信息
    @RequestMapping(value = "getUserInfo.do",method = RequestMethod.POST)
    public ServerResponse<User> getUserInfo(HttpSession session){
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        return user == null ? ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage()) :
                              ServerResponse.createBySuccess(user);
    }

    // 获取密保问题
    @RequestMapping(value = "forgotGetQuestion.do",method = RequestMethod.POST)
    public ServerResponse<String> forgotGetQuestion(String username) {
        return iUserService.selectQuestion(username);
    }

    // 校验输入答案与密保问题答案是否一致
    @RequestMapping(value = "checkedAnswer.do",method = RequestMethod.POST)
    public ServerResponse<String> checkedAnswer(String username,String question,String answer) {
        return iUserService.checkedAnswer(username, question, answer);
    }

    // 找回密码
    @RequestMapping(value = "forgotRestPassword.do",method = RequestMethod.POST)
    public ServerResponse<String> forgotRestPassword(String username,String newPassword,String forgotToken) {
        return iUserService.forgotResetPassword(username, newPassword, forgotToken);
    }

    // 登录状态，更新密码
    @RequestMapping(value = "restPassword.do",method = RequestMethod.POST)
    public ServerResponse<String> restPassword(HttpSession session,String oldPassword,String newPassword) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        return iUserService.resetPassword(user, oldPassword, newPassword);
    }

    // 更新用户信息
    @RequestMapping(value = "updateInformation.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> updateInformation(HttpSession session,User user) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        user.setId(currentUser.getId());
        user.setUsername(currentUser.getUsername());
        // 更新成功更新 session
        ServerResponse<User> updateUser = iUserService.updateInformation(user);
        if (updateUser.isSuccess()) {
            session.setAttribute(Const.CURRENT_USER,updateUser.getData());
        }
        return updateUser;
    }

    // 获取用户信息
    @RequestMapping(value = "getInformation.do", method = RequestMethod.POST)
    public ServerResponse<User> getInformation(HttpSession session) {
        // 判断用户是否登陆
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        return iUserService.getInformation(user.getId());
    }

//    @RequestMapping(value = "test.do", method = RequestMethod.GET)
//    public ServerResponse<String> test(){
//        return ServerResponse.createBySuccess("测试成功");
//    }

}

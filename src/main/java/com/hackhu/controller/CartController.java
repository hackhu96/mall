package com.hackhu.controller;

import com.hackhu.common.Const;
import com.hackhu.common.ServerResponse;
import com.hackhu.enums.ResponseCode;
import com.hackhu.pojo.User;
import com.hackhu.service.ICartService;
import com.hackhu.vo.CartVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/cart/")
public class CartController {

    @Autowired
    private ICartService iCartService;

    @RequestMapping(value = "add.do")
    public ServerResponse<CartVO> add(HttpSession session, Integer productId, Integer count) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        return iCartService.add(user.getId(), productId, count);
    }

    @RequestMapping(value = "update.do")
    public ServerResponse<CartVO> update(HttpSession session, Integer productId, Integer count) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        return iCartService.update(user.getId(), productId, count);
    }

    @RequestMapping(value = "delete.do")
    public ServerResponse<CartVO> delete(HttpSession session, String productIds) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        return iCartService.deleteProduct(user.getId(), productIds);
    }

    @RequestMapping(value = "list.do")
    public ServerResponse<CartVO> list(HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        return iCartService.list(user.getId());
    }

    // 全选商品
    @RequestMapping(value = "selectAll.do")
    public ServerResponse<CartVO> selectAll(HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        return iCartService.selectOrUnSelectAll(user.getId(), Const.CartStatus.CHECKED);
    }

    // 全反选商品
    @RequestMapping(value = "unSelectAll.do")
    public ServerResponse<CartVO> unSelectAll(HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        return iCartService.selectOrUnSelectAll(user.getId(),Const.CartStatus.UN_CHECKED);
    }

    // 单独选中
    @RequestMapping(value = "select.do")
    public ServerResponse<CartVO> select(HttpSession session,Integer productId) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        return iCartService.selectOrUnSelect(user.getId(), productId,Const.CartStatus.CHECKED);
    }

    // 单独反选
    @RequestMapping(value = "unSelect.do")
    public ServerResponse<CartVO> unSelect(HttpSession session, Integer productId) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        return iCartService.selectOrUnSelect(user.getId(), productId,Const.CartStatus.UN_CHECKED);
    }

    // 获取购物车当前产品数量
    @RequestMapping(value = "getCartProductCount.do")
    public ServerResponse<Integer> getCartProductCount(HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        return iCartService.getCartProductCount(user.getId());
    }

}

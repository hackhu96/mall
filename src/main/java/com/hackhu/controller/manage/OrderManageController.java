package com.hackhu.controller.manage;

import com.github.pagehelper.PageInfo;
import com.hackhu.common.Const;
import com.hackhu.common.ServerResponse;
import com.hackhu.enums.ResponseCode;
import com.hackhu.pojo.User;
import com.hackhu.service.IOrderService;
import com.hackhu.service.IUserService;
import com.hackhu.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/manage/order/")
public class OrderManageController {
    @Autowired
    private IOrderService iOrderService;
    @Autowired
    private IUserService iUserService;

    private ServerResponse valid(HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        return iUserService.checkAdminRole(user).isSuccess() ? ServerResponse.createBySuccess() :
                ServerResponse.createByError(ResponseCode.NO_ADMIN.getMessage());
    }

    @RequestMapping(value = "orderList.do")
    public ServerResponse<PageInfo> orderList(HttpSession session, @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum, @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        ServerResponse serverResponse = valid(session);
        if (!serverResponse.isSuccess()) {
            return serverResponse;
        }
        return iOrderService.manageList(pageNum, pageSize);
    }

    @RequestMapping(value = "orderDetail.do")
    public ServerResponse<OrderVO> orderDetail(HttpSession session,@RequestParam(value = "orderNo") Long orderNo) {
        ServerResponse serverResponse = valid(session);
        if (!serverResponse.isSuccess()) {
            return serverResponse;
        }
        return iOrderService.orderDetail(orderNo);
    }

    @RequestMapping(value = "search.do")
    public ServerResponse<PageInfo> search(HttpSession session,@RequestParam(value = "orderNo") Long orderNo,
                                           @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        ServerResponse serverResponse = valid(session);
        if (!serverResponse.isSuccess()) {
            return serverResponse;
        }
        return iOrderService.manageSearch(orderNo, pageNum, pageSize);
    }

    @RequestMapping(value = "sendGood.do")
    public ServerResponse<String> sendGood(HttpSession session,@RequestParam(value = "orderNo") Long orderNo) {
        ServerResponse serverResponse = valid(session);
        if (!serverResponse.isSuccess()) {
            return serverResponse;
        }
        return iOrderService.manageSendGoods(orderNo);
    }

}

package com.hackhu.controller.manage;

import com.google.common.collect.Maps;
import com.hackhu.common.Const;
import com.hackhu.common.ServerResponse;
import com.hackhu.enums.ResponseCode;
import com.hackhu.pojo.Product;
import com.hackhu.pojo.User;
import com.hackhu.service.IFileService;
import com.hackhu.service.IProductService;
import com.hackhu.service.IUserService;
import com.hackhu.utils.PropertiesUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/manage/product/")
public class ProductManageController {
    @Autowired
    private IUserService iUserService;
    @Autowired
    private IProductService iProductService;
    @Autowired
    private IFileService iFileService;

    @RequestMapping(value = "productSave.do")
    public ServerResponse<String> productSave(HttpSession session, Product product) {
        ServerResponse result = valid(session);
        if (result.isSuccess()) {
            return iProductService.updateProductStatus(product.getId(), product.getStatus());
        } else { // 未通过验证
            return result;
        }
    }

    @RequestMapping(value = "productList.do")
    public ServerResponse productList(HttpSession session, @RequestParam(value = "pageNum",defaultValue = "1") int pageNum,
                                              @RequestParam(value = "pageSize",defaultValue = "5") int pageSize ) {
        ServerResponse result = valid(session);
        if (result.isSuccess()) {
            return iProductService.getProductList(pageNum, pageSize);
        } else { // 未通过验证
            return result;
        }
    }

    @RequestMapping(value = "search.do")
    public ServerResponse productSearch(HttpSession session, String productName, Integer productId, @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "5") int pageSize) {
        ServerResponse result = valid(session);
        if (result.isSuccess()) {
            return iProductService.searchProduct(productName, productId,pageNum,pageSize);
        } else { // 未通过验证
            return result;
        }
    }

    @RequestMapping(value = "upload.do")
    public ServerResponse upload(HttpSession session, @RequestParam(value = "upload_file", required = false) MultipartFile file, HttpServletRequest request) {
        ServerResponse result = valid(session);
        if (result.isSuccess()) {
            String path = request.getSession().getServletContext().getRealPath("upload");
            String targetFileName = iFileService.upload(file, path);
            String url = PropertiesUtil.getProperty("ftp.server.http.prefix") + targetFileName;
            Map fileMap = Maps.newHashMap();
            fileMap.put("uri", targetFileName);
            fileMap.put("url", url);
            return ServerResponse.createBySuccess(fileMap);
        } else { // 未通过验证
            return result;
        }
    }

    @RequestMapping(value = "richText_img_upload.do")
    public Map richTextImgUpload(HttpSession session, @RequestParam(value = "upload_file", required = false) MultipartFile file, HttpServletRequest request, HttpServletResponse response) {
        Map resultMap = Maps.newHashMap();
        ServerResponse result = valid(session);
        if (result.isSuccess()) {
            String path = request.getSession().getServletContext().getRealPath("upload");
            String targetFileName = iFileService.upload(file, path);
            String url = PropertiesUtil.getProperty("ftp.server.http.prefix") + targetFileName;
            if (StringUtils.isBlank(targetFileName)) {
                resultMap.put("success", false);
                resultMap.put("msg", ResponseCode.NEED_LOGIN.getMessage());
                return resultMap;
            }
            resultMap.put("success", true);
            resultMap.put("msg", "上传成功");
            resultMap.put("file_path", url);
            // 设置响应头
            response.addHeader("Access-Control-Allow-Headers", "X-File-Name");
            return resultMap;
        } else { // 未通过验证
            resultMap.put("success", false);
            resultMap.put("msg", ResponseCode.NEED_LOGIN.getMessage());
            return resultMap;
        }
    }
        // 验证 session 合法性
    private ServerResponse valid(HttpSession session){
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        if (iUserService.checkAdminRole(user).isSuccess()) {
            return ServerResponse.createBySuccess();
        } else { // 判断权限
            return ServerResponse.createByError(ResponseCode.NO_ADMIN.getMessage());
        }
    }
}

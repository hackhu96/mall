package com.hackhu.controller.manage;

import com.hackhu.common.Const;
import com.hackhu.common.ServerResponse;
import com.hackhu.enums.ResponseCode;
import com.hackhu.pojo.Category;
import com.hackhu.pojo.User;
import com.hackhu.service.ICategoryService;
import com.hackhu.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/manage/category")
public class CategoryManageController {
    @Autowired
    private IUserService iUserService;
    @Autowired
    private ICategoryService iCategoryService;

    private ServerResponse<String> valid(HttpSession session){
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getMessage());
        }
        ServerResponse<String> response = iUserService.checkAdminRole(user);
        if (!response.isSuccess()) {
            return ServerResponse.createByError(ResponseCode.NO_ADMIN.getMessage());
        }
        return ServerResponse.createBySuccess();
    }
    @RequestMapping(value = "addCategory.do",method = RequestMethod.GET)
    public ServerResponse<String> addCategory(HttpSession session, String category, @RequestParam(value = "parentId",
            defaultValue = "0")int parentId,String categoryName ){
        if (!valid(session).isSuccess()) {
            return valid(session);
        }
        return iCategoryService.addCategory(parentId, categoryName);
    }

    @RequestMapping(value = "updateCategory.do",method = RequestMethod.GET)
    public ServerResponse<String> updateCategoryName(HttpSession session, Integer categoryId,String categoryName){
        if (!valid(session).isSuccess()) {
            return valid(session);
        }
        return iCategoryService.updateCategoryName(categoryId,categoryName);
    }

    @RequestMapping(value = "getChildrenParallelCategory.do",method = RequestMethod.GET)
    public ServerResponse getChildrenParallelCategory(HttpSession session, Integer categoryId, String categoryName){
        if (!valid(session).isSuccess()) {
            return valid(session);
        }
        // 非递归获取子分类
        return iCategoryService.getCategoryChildByParentId(categoryId);
    }

    @RequestMapping(value = "getCategoryAndDeepChildrenParallelCategory.do",method = RequestMethod.GET)
    public ServerResponse getCategoryAndDeepChildrenParallelCategory(HttpSession session, Integer categoryId, String categoryName){
        if (!valid(session).isSuccess()) {
            return valid(session);
        }
        // 递归获取子分类
        return iCategoryService.getCategoryAndDeepChildrenParallelCategory(categoryId);
    }

}

package com.hackhu.utils;

import java.math.BigDecimal;

import static java.math.BigDecimal.ROUND_HALF_UP;

public class BigDecimalUtil {
    private BigDecimalUtil(){}

    public static BigDecimal add(double num1, double num2) {
        BigDecimal v1 = new BigDecimal(num1);
        BigDecimal v2 = new BigDecimal(num2);
        return v1.add(v2);
    }

    public static BigDecimal sub(double num1, double num2) {
        BigDecimal v1 = new BigDecimal(num1);
        BigDecimal v2 = new BigDecimal(num2);
        return v1.subtract(v2);
    }
    public static BigDecimal mul(double num1, double num2) {
        BigDecimal v1 = new BigDecimal(num1);
        BigDecimal v2 = new BigDecimal(num2);
        return v1.multiply(v2);
    }

    public static BigDecimal div(double num1, double num2) {
        BigDecimal v1 = new BigDecimal(num1);
        BigDecimal v2 = new BigDecimal(num2);
        return v1.divide(v2,2,ROUND_HALF_UP); // 四舍五入，保留两位小数
    }
}

package com.hackhu.utils;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketException;
import java.util.List;

@Data
@NoArgsConstructor
public class FTPUtil {
    private static String ftpIp = PropertiesUtil.getProperty("ftp.server.ip");
    private static String ftpUser = PropertiesUtil.getProperty("ftp.user");
    private static String ftpPwd = PropertiesUtil.getProperty("ftp.pwd");
    private static Logger logger = LoggerFactory.getLogger(FTPUtil.class);

    private String ip;
    private int port;
    private String user;
    private String pwd;
    private FTPClient ftpClient;

    public FTPUtil(String ip,int port,String user,String pwd){
        this.ip = ip;
        this.port = port;
        this.user = user;
        this.pwd = pwd;
    }

    public static boolean uploadFile(List<File> files) {
        FTPUtil ftpUtil = new FTPUtil(ftpIp, 21, ftpUser, ftpPwd);
        logger.info("开始连接 ftp 服务器");
        boolean result = ftpUtil.uploadFile("img", files);
        logger.info("文件上传结果：{}", result);
        return result;
    }

    private boolean uploadFile(String remotePath,List<File> fileList){
        boolean uploaded = true;
        FileInputStream fis = null;
        // 连接 ftp 服务器
        if (connectionServer(ip, port, user, pwd)) {
            try {
                ftpClient.changeWorkingDirectory(remotePath);
                ftpClient.setBufferSize(1024);
                ftpClient.setControlEncoding("UTF-8");
                ftpClient.enterLocalPassiveMode();
                for (File file : fileList) {
                    fis = new FileInputStream(file);
                    ftpClient.storeFile(file.getName(), fis);
                }
            } catch (IOException e) {
                uploaded = false;
                logger.error("文件上传异常", e);
            }finally {
                // 释放资源
                try {
                    fis.close();
                    ftpClient.disconnect();
                } catch (IOException e) {
                    logger.error("资源关闭异常", e);
                }
            }
        }
        return uploaded;
    }

    private boolean connectionServer(String ip, int port, String user, String pwd) {
        boolean isSuccess = false;
        ftpClient = new FTPClient();
        try {
            ftpClient.connect(ip);
            isSuccess = ftpClient.login(user, pwd);
        }catch (IOException e) {
            logger.error("FTP 连接异常", e);
        }
        return isSuccess;
    }

}

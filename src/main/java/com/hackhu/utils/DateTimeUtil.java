package com.hackhu.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import org.joda.time.format.DateTimeFormatter;
import java.util.Date;

public class DateTimeUtil {

    private static final String STANDARD_FORMAT = "yyyy-MM-dd HH:mm:ss";

    // 字符串转换为对应格式的日期
    public static Date strToDate(String dateTimeStr,String dateFormat){
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(dateFormat);
        return dateTimeFormatter.parseDateTime(dateTimeStr).toDate();
    }

    // 字符串转换为对应格式的日期
    public static Date strToDate(String dateTimeStr){
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(STANDARD_FORMAT);
        return dateTimeFormatter.parseDateTime(dateTimeStr).toDate();
    }

    // 日期转换为字符串
    public static String dateToStr(Date date, String format) {
        if (date == null) {
            return "";
        }
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(format);
    }

    // 日期转换为字符串
    public static String dateToStr(Date date) {
        if (date == null) {
            return "";
        }
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(STANDARD_FORMAT);
    }
}

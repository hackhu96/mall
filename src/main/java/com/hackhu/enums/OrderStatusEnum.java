package com.hackhu.enums;

import lombok.Getter;

@Getter
public enum OrderStatusEnum {
    CANCELED(1,"已取消"),
    NO_PAY(10, "未付款"),
    PAID(20,"已付款"),
    SHIPPING(30, "已发货"),
    ORDER_SUCCESS(40, "订单已完成"),
    ORDER_CLOSE(50,"订单关闭")
    ;

    private final int code;
    private final String msg;

    OrderStatusEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static OrderStatusEnum codeOf(int code) {
        for (OrderStatusEnum orderStatusEnum : OrderStatusEnum.values()) {
            if (orderStatusEnum.code == code) {
                return orderStatusEnum;
            }
        }
        throw new RuntimeException("未找到代码所对应的订单状态");
    }
}

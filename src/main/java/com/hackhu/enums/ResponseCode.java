package com.hackhu.enums;

import lombok.Getter;

@Getter
public enum ResponseCode {

    SUCCESS(0,"已登陆"),
    ERROR(1, "登陆出错"),
    NEED_LOGIN(2,"请先登陆"),
    ILLEGAL_ARGUMENT(3,"非法参数"),
    NO_EXIST(4,"用户不存在"),
    NO_ADMIN(4,"不具备管理员权限");
    private final int code;
    private final String message;

    private ResponseCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}

package com.hackhu.enums;

import lombok.Getter;

@Getter
public enum TradeStatusEnum {
    WAIT_BUYER_PAY(0, "交易创建，等待买家付款"),
    TRADE_CLOSED(10, "未付款交易超时关闭，或支付完成后全额退款"),
    TRADE_SUCCESS(20, "交易支付成功"),
    TRADE_FINISHED(30,"交易结束，不可退款"),
    RESPONSE_SUCCESS(40,"响应成功"),
    RESPONSE_FAILED(50,"响应失败");
    private final int code;
    private final String msg;

    TradeStatusEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}

package com.hackhu.enums;

import lombok.Getter;

@Getter
public enum ProductStatusEnum {

    ON_SALE(1,"正在销售"),
    TAKE_DOWN(2,"已下架");

    private final int code;
    private final String msg;

    ProductStatusEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}

package com.hackhu.enums;

import lombok.Getter;

@Getter
public enum PayPlatformEnum {
    ALIPAY(10, "支付宝"),
    WEIXIN(20,"微信支付");
    private final int code;
    private final String msg;

    PayPlatformEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}

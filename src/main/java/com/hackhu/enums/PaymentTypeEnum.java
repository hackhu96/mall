package com.hackhu.enums;

import lombok.Getter;

@Getter
public enum PaymentTypeEnum {
    ONLINE_PAY(1,"在线支付");

    private final int code;
    private final String msg;

    PaymentTypeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static PaymentTypeEnum codeOf(int code) {
        for (PaymentTypeEnum paymentTypeEnum : PaymentTypeEnum.values()) {
            if (paymentTypeEnum.code == code) {
                return paymentTypeEnum;
            }
        }
        throw new  RuntimeException("未找到代码所对应枚举");
    }
}

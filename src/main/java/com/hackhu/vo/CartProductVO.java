package com.hackhu.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CartProductVO {
    private Integer id;
    private Integer userId;
    private Integer productId;
    private Integer quantity; // 购物车中产品数量
    private String productName;
    private String productSubTitle;
    private String productMainImage;
    private BigDecimal productPrice;
    private Integer productStatus;
    private BigDecimal productTotalPrice;
    private Integer productStocks;
    private Integer productChecked;
    private String limitQuantity; // 限制产品最大数量
}

package com.hackhu.vo;

import com.hackhu.pojo.OrderDetail;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderProductVO {
    private List<OrderDetailVO> orderDetailVOS;
    private BigDecimal productTotalPrice;
    private String imageHost;
}

package com.hackhu.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductListVO {
    private Integer id;
    private Integer categoryId;
    private String name;
    private String subTitle;
    private String mainImage;
    private String subImages;
    private BigDecimal price;
    private Integer status;
    private String imageHost;
}

package com.hackhu.vo;

import lombok.Data;
import lombok.Getter;

@Data
public class ShippingVO {
    private String receiverName;

    private String receiverTelephone;

    private String receiverMobile;

    private String receiverProvince;

    private String receiverCity;

    private String receiverDistrict;

    private String receiverAddress;

    private String receiverZip;
}

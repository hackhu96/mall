package com.hackhu.vo;

import lombok.Data;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderDetailVO {
    private Long orderNo;

    private Integer productId;

    private String productName;

    private String productImage;

    private BigDecimal currentInitPrice;

    private Integer quantity;

    private BigDecimal totalPrice;

    private String createTime;
}

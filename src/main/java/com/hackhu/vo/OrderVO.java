package com.hackhu.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class OrderVO {
    private Long orderNo;

   private BigDecimal payment;

    private Integer paymentType;

    private String paymentTypeDesc;

    private Integer postage;

    private Integer status;

    private String statusDesc;

    private String paymentTime;

    private String sendTime;

    private String endTime;

    private String createTime;

    private String closeTime;

    // 订单明细
    private List<OrderDetailVO> orderDetailVOS;

    private String imageHost;

    private Integer shippingId;

    private ShippingVO shippingVO;

    private String receiverName;
}

package com.hackhu.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hackhu.enums.ResponseCode;
import lombok.Getter;

import java.io.Serializable;

@Getter
// 保证序列化 Json 时，key 也会消失
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ServerResponse<T> implements Serializable {
    private int status;
    private String message;
    private T data;

    private ServerResponse(int status){
        this.status = status;
    }

    private ServerResponse(int status, T data) {
        this.status = status;
        this.data = data;
    }

    private ServerResponse(int status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    // 优先匹配
    private ServerResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }

    // 在 Json 序列化中忽略
    @JsonIgnore
    public boolean isSuccess() {
        return this.status == ResponseCode.SUCCESS.getCode();
    }

    public static <T> ServerResponse<T> createBySuccess(){
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode());
    }

    public static <T> ServerResponse<T> createBySuccess(String message){
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode(),message);
    }

    public static <T> ServerResponse<T> createBySuccess(T data){
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode(), data);
    }

    public static <T> ServerResponse<T> createBySuccess(String message,T data){
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode(), message, data);
    }

    public static <T> ServerResponse<T> createByError(){
        return new ServerResponse<>(ResponseCode.ERROR.getCode());
    }

    public static <T> ServerResponse<T> createByError(String message){
        return new ServerResponse<>(ResponseCode.ERROR.getCode(),message);
    }

    public static <T> ServerResponse<T> createByError(int code,String message){
        return new ServerResponse<>(code,message);
    }

    public static <T> ServerResponse<T> createByError(T data){
        return new ServerResponse<>(ResponseCode.ERROR.getCode(), data);
    }

    public static <T> ServerResponse<T> createByError(String message,T data){
        return new ServerResponse<>(ResponseCode.ERROR.getCode(), message, data);
    }
}

package com.hackhu.common;

import com.google.common.collect.Sets;

import java.util.Set;

public class Const {
    public static final String CURRENT_USER = "currentUser";
    public static final String EMAIL = "email";
    public static final String USERNAME = "username";
    public static final String TOKEN_PREFIX = "token_";
    public interface ProductListOrderBy{
        Set<String> PRICE_ASC_DESC = Sets.newHashSet("price_asc", "price_desc");
    }
    public interface CartStatus{
        int UN_CHECKED = 0; // 未被选中
        int CHECKED = 1; // 被选中
        String LIMIT_NUM_FAIL = "库存限制失败";
        String LIMIT_NUM_SUCCESS = "库存限制成功";
    }
    public interface Role{
        int ROLE_CUSTOMER = 0; // 普通用户
        int ROLE_ADMIN = 1; // 管理员
    }
}

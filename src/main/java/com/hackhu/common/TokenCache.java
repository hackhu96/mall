package com.hackhu.common;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class TokenCache {

    private static Logger logger = LoggerFactory.getLogger(TokenCache.class);

    // LRU 处理缓存数据
    private static LoadingCache<String, String> localCache = CacheBuilder.newBuilder().initialCapacity(10000).maximumSize(1000).
                                                            expireAfterAccess(12, TimeUnit.HOURS).build(new CacheLoader<String, String>() {
        @Override
        public String load(String s) throws Exception {
            return "null";
        }
    });

    public static void setKey(String key,String value) {
        localCache.put(key,value);
    }
    public static String getKey(String key){
        String value = null;
        try {
            value = localCache.get(key);
            return "null".equals(value) ? null : value;
        } catch (ExecutionException e) {
            logger.error("localCache get error"+ e.getMessage());
        }
        return null;
    }
}
